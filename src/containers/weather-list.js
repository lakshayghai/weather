import React, { Component } from 'react';
import { connect } from 'react-redux';
import WeatherCharts from '../components/weather-chart'
import GoogleMap from '../components/google-map';

class WeatherList extends Component {
    renderCity() {
        console.log(this.props.weather);
        return this.props.weather.map( weather => {
            const temperatures = weather.list.map(
                temps => {
                    return temps.main.temp;
                }
            )
            const pressures = weather.list.map(
                temps => {
                    return temps.main.pressure;
                }
            )
            const humidities = weather.list.map(
                temps => {
                    return temps.main.humidity;
                }
            )

            const { lat, lon } = weather.city.coord;
            return (
              <tr key={weather.city.id}>
                  <td><GoogleMap lat={lat} lon={lon} /></td>
                  <td><WeatherCharts data={temperatures} color="blue"></WeatherCharts></td>
                  <td><WeatherCharts data={pressures} color="green"></WeatherCharts></td>
                  <td><WeatherCharts data={humidities} color="red"></WeatherCharts></td>
              </tr>
            );
        })
    }

    render() {
        return (
          <table className="table table-hover">
              <thead>
                  <tr>
                      <th>City</th>
                      <th>Temperature</th>
                      <th>Pressure</th>
                      <th>Humidity</th>
                  </tr>
              </thead>
              <tbody>
              {this.renderCity()}
              </tbody>
          </table>
        );
    }
}

function mapStateToProps({weather}) {
    return {
        weather
    };
}

export default connect(mapStateToProps)(WeatherList);