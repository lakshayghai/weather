import React from 'react';
import { Sparklines, SparklinesLine } from 'react-sparklines';

export default function(props){
    return (
        <Sparklines data={props.data} width={180} height={120}>
            <SparklinesLine color={props.color}>
            </SparklinesLine>
        </Sparklines>
    );
}