import { ROOT_URL, FETCH_WEATHER } from '../constants/constants';
import axios from 'axios';

export function fetchWeather(city) {

    const url = `${ROOT_URL}&q=${city},us`;
    const request = axios.get(url);

    return {
        type: FETCH_WEATHER,
        payload: request
    }
}